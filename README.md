# vue-image-verify

## 效果
![effect test](https://gitee.com/fzh163/image-verify/raw/master/examples/images/test.gif)

## 安装
```
npm install vue-f-image-verify konva -S
```

## 使用

src/main.js
```
import ImageVerify from 'vue-f-image-verify'
import 'vue-f-image-verify/lib/image-verify.css'
import Konva from 'konva'
Vue.use(ImageVerify)
Vue.use(Konva)
```

test.vue
```
<template>
    <div>
        <image-verify :images="images" v-if="isShow">
            <template slot="button">
                <img :src="button" alt="" >
            </template>
        </image-verify>
        <button @click="refresh">刷新</button>
    </div>
</template>

<script>
import image1 from '@/static/image1.png'
import image2 from '@/static/image2.png'
import button from '@/static/button.png'

export default {
    name: 'Login',
    data() {
        return {
            images: [image1, image2],
            button,
            isShow: true
        }
    },
    methods: {
        // 刷新
        refresh () {
            this.isShow = false
            this.$nextTick(() => {
                this.isShow = true
            })
        }
    }
}
</script>
```

## 配置

##### 属性
参数 | 说明 | 数据类型 | 可选值 | 默认值
--- | --- | --- | --- | ---
images | 用来显示的图片地址数组 | array | --- | ---
spacing | 移动的拼图和缺口距离小于该数值通过验证 | number | --- | 5
sliderText | 滑块提示语 | String | --- | 拖动滑块完成上方拼图
showValidateText | 是否展示验证后的提示语 | Boolean | --- | true
validateFailText | 验证失败提示语 | String | --- | 拖动滑块将悬浮图像正确拼合
validateSucText | 验证成功提示语 | String | --- | 验证通过,正在登录...
resumeSpeed | 验证失败后拼图回弹速度 | Number | 大于0 | 5
failDelay | 验证失败后拼图回弹延时 | Number | 大于0 | 1500
width | 图片区域宽度 | Number | \>=100 | 280
height | 图片区域高度 | Number | \>=100 | 180
puzzleSize | 拼图宽高 | Number | --- | 40
outWidth | 拼图凸起部分或凹陷部分的宽度 | Number | --- | 12
outHeight | 拼图凸起部分或凹陷部分的高度 | Number | --- | 6
sliderBtnSize | 滑块宽高 | Number | \>36 | 60
padding | 初始拼图离边缘距离 | Number | \>0 | 10
randomX | 拼图终点位置X | Number | 大于(padding+puzzleSize) 小于(width-padding) | 随机
randomY | 拼图终点位置Y | Number | 大于等于padding 小于(height-padding) | 随机
puzzleType | 拼图类型及朝向 | Number | 1,2,3...16 | 随机

##### slot
name | 说明
--- | ---
button | 滑块图片或者svg等
foot | 底部添加内容（如关闭刷新等）

##### 事件
name | 说明 | 参数
--- | --- | ---
validate | 移动拼图后验证结果时触发 参数 1.是否通过 参数 2. 拼图位置 | isPass, { x, y }
validated | 验证失败后，回弹动画结束后触发 | ---
