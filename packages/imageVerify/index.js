// 导入组件，组件必须声明 name
import ImageVerify from './src/image-verify.vue'

// 为组件提供 install 安装方法，供按需引入
ImageVerify.install = function (Vue) {
  Vue.component(ImageVerify.name, ImageVerify)
}

// 默认导出组件
export default ImageVerify
