(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["image-verify"] = factory();
	else
		root["image-verify"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "3825":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_image_verify_vue_vue_type_style_index_0_id_d49aee4a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("bd8a");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_image_verify_vue_vue_type_style_index_0_id_d49aee4a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_image_verify_vue_vue_type_style_index_0_id_d49aee4a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_image_verify_vue_vue_type_style_index_0_id_d49aee4a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "8875":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// addapted from the document.currentScript polyfill by Adam Miller
// MIT license
// source: https://github.com/amiller-gh/currentScript-polyfill

// added support for Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1620505

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
}(typeof self !== 'undefined' ? self : this, function () {
  function getCurrentScript () {
    if (document.currentScript) {
      return document.currentScript
    }
  
    // IE 8-10 support script readyState
    // IE 11+ & Firefox support stack trace
    try {
      throw new Error();
    }
    catch (err) {
      // Find the second match for the "at" string to get file src url from stack.
      var ieStackRegExp = /.*at [^(]*\((.*):(.+):(.+)\)$/ig,
        ffStackRegExp = /@([^@]*):(\d+):(\d+)\s*$/ig,
        stackDetails = ieStackRegExp.exec(err.stack) || ffStackRegExp.exec(err.stack),
        scriptLocation = (stackDetails && stackDetails[1]) || false,
        line = (stackDetails && stackDetails[2]) || false,
        currentLocation = document.location.href.replace(document.location.hash, ''),
        pageSource,
        inlineScriptSourceRegExp,
        inlineScriptSource,
        scripts = document.getElementsByTagName('script'); // Live NodeList collection
  
      if (scriptLocation === currentLocation) {
        pageSource = document.documentElement.outerHTML;
        inlineScriptSourceRegExp = new RegExp('(?:[^\\n]+?\\n){0,' + (line - 2) + '}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*', 'i');
        inlineScriptSource = pageSource.replace(inlineScriptSourceRegExp, '$1').trim();
      }
  
      for (var i = 0; i < scripts.length; i++) {
        // If ready state is interactive, return the script tag
        if (scripts[i].readyState === 'interactive') {
          return scripts[i];
        }
  
        // If src matches, return the script tag
        if (scripts[i].src === scriptLocation) {
          return scripts[i];
        }
  
        // If inline source matches, return the script tag
        if (
          scriptLocation === currentLocation &&
          scripts[i].innerHTML &&
          scripts[i].innerHTML.trim() === inlineScriptSource
        ) {
          return scripts[i];
        }
      }
  
      // If no match, return null
      return null;
    }
  };

  return getCurrentScript
}));


/***/ }),

/***/ "bd8a":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (true) {
    var getCurrentScript = __webpack_require__("8875")
    currentScript = getCurrentScript()

    // for backward compatibility, because previously we directly included the polyfill
    if (!('currentScript' in document)) {
      Object.defineProperty(document, 'currentScript', { get: getCurrentScript })
    }
  }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"10c26688-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/imageVerify/src/image-verify.vue?vue&type=template&id=d49aee4a&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{ref:"img_verify",staticClass:"img-verify",class:(_vm.isCanDrag || _vm.isPassed) ? 'hide' : 'vibration'},[_c('div',{staticClass:"warp"},[_c('div',{staticClass:"image-verify-canvas-box"},[_c('div',{attrs:{"id":"image-verify-canvas"}}),(!_vm.isCanvasLoaded)?_c('div',{staticClass:"canvas-tip-box",style:({ width: _vm.width + 'px', height: _vm.height + 'px', lineHeight: _vm.height + 'px' })},[_vm._v("正在加载...")]):_vm._e(),(_vm.showValidateText)?_c('div',[_c('div',{staticClass:"error-text",class:(_vm.isCanDrag || _vm.isPassed) ? 'hide' : 'show'},[_vm._v(_vm._s(_vm.validateFailText))]),_c('div',{staticClass:"success-text",class:_vm.isPassed ? 'show' : 'hide'},[_vm._v(_vm._s(_vm.validateSucText))])]):_vm._e()]),_c('div',{staticClass:"slider-box"},[_c('div',{staticClass:"slider-main"},[_c('div',{staticClass:"slider-bar noselect",class:_vm.sliderDragAble ? 'hide' : 'show',style:({ padding: '0 ' + _vm.padding + 'px' })},[_c('div',{staticClass:"slider-bar-text"},[_vm._v(_vm._s(_vm.sliderText))])]),_c('div',{ref:"slider_button",staticClass:"slider-button",style:(_vm.sliderBtnStyle),on:{"mousedown":function($event){$event.preventDefault();return _vm.sliderStart($event)},"touchstart":function($event){$event.preventDefault();return _vm.touchStart($event)}}},[_vm._t("button")],2)])])]),_vm._t("foot")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./packages/imageVerify/src/image-verify.vue?vue&type=template&id=d49aee4a&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./packages/imageVerify/src/image-verify.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var image_verifyvue_type_script_lang_js_ = ({
  name: 'ImageVerify',
  data () {
    return {
      // 用于验证的图片
      imageObj: null,
      // 随机拼图的位置
      random: {
        x: 0,
        y: 0
      },
      intervalId: null, // 回弹定时器
      timeoutId: null, // 回弹定时器
      isCanvasLoaded: false,
      sliderDragAble: false, // 是否处于拖动状态
      isCanDrag: true, // 是否可以被拖动
      isPassed: false, // 是否验证通过
      // sliderOffsetX: 0, // 鼠标第一次点击滑块时, 在滑块中的位置
      sliderClientX: 0, // 鼠标第一次点击滑块时, 鼠标相对屏幕的位置
      clipGroup: null, // konva中组的实例对象(拼图)
      layer: null // konva中层的实例对象
    }
  },
  props: {
    // 图片数组，默认必须传
    images: {
      type: Array,
      required: true
    },
    // 移动的拼图和缺口距离小于该数值通过验证
    spacing: {
      type: Number,
      default: 5
    },
    // 滑块提示语
    sliderText: {
      type: String,
      default: '拖动滑块完成上方拼图'
    },
    // 验证失败提示语
    validateFailText: {
      type: String,
      default: '拖动滑块将悬浮图像正确拼合'
    },
    // 验证成功提示语
    validateSucText: {
      type: String,
      default: '验证通过,正在登录...'
    },
    // 是否展示验证后的提示语
    showValidateText: {
      type: Boolean,
      default: true
    },
    // 验证失败后，滑块回弹延迟
    failDelay: {
      type: Number,
      default: 1500
    },
    // 画布宽度
    width: {
      type: Number,
      default: 280,
      validator: function (value) {
        return value >= 100
      }
    },
    // 画布高度
    height: {
      type: Number,
      default: 180,
      validator: function (value) {
        return value >= 100
      }
    },
    // 拼图宽高
    puzzleSize: {
      type: Number,
      default: 40
    },
    // 凸起部分和凹陷部分的宽度
    outWidth: {
      type: Number,
      default: 12
    },
    // 凸起部分和凹陷部分的高度
    outHeight: {
      type: Number,
      default: 6
    },
    // 验证失败后拼图回弹速度
    resumeSpeed: {
      type: Number,
      default: 5
    },
    // 滑块宽高
    sliderBtnSize: {
      type: Number,
      default: 60,
      validator: function (value) {
        return value > 36
      }
    },
    // 拼图离边缘距离
    padding: {
      type: Number,
      default: 10,
      validator: function (value) {
        return value > 0
      }
    },
    // 拼图终点位置X
    randomX: {
      type: Number,
      validator: function (value) {
        return value > 0
      }
    },
    // 拼图终点位置Y
    randomY: {
      type: Number,
      validator: function (value) {
        return value > 0
      }
    },
    // 拼图类型
    puzzleType: {
      type: Number,
      default: Math.floor(Math.random() * 16) + 1,
      validator: function (value) {
        return value >= 1 && value <= 16
      }
    }
  },
  computed: {
    leftGap () {
      return (this.sliderBtnSize - this.puzzleSize) / 2
    },
    sliderBtnStartX () {
      return -this.leftGap + this.padding
    },
    sliderBtnStyle () {
      return {
        width: this.sliderBtnSize + 'px',
        height: this.sliderBtnSize + 'px',
        top: (-(this.sliderBtnSize - 36) / 2) + 'px',
        left: this.sliderBtnStartX + 'px'
      }
    }
  },
  created () {
    this.initRandom()
  },
  mounted () {
    this.initImage()
  },
  beforeDestroy () {
    if (this.intervalId != null) {
      clearInterval(this.intervalId)
    }
    if (this.timeoutId != null) {
      clearTimeout(this.timeoutId)
    }
  },
  methods: {
    /* 初始化拼图位置 */
    initRandom () {
      if (this.randomX) {
        this.random.x = this.randomX > (this.padding + this.puzzleSize) ? this.randomX : (this.padding + this.puzzleSize)
        this.random.x = this.random.x <= (this.width - this.puzzleSize - this.padding) ? this.random.x : (this.width - this.puzzleSize - this.padding)
      } else {
        this.random.x = Math.floor(Math.random() * (this.width - this.puzzleSize * 3 - this.padding * 2) + this.padding + this.puzzleSize * 2)
      }
      if (this.randomY) {
        this.random.y = this.randomY >= this.padding ? this.randomY : this.padding
        this.random.y = this.random.y <= (this.height - this.puzzleSize - this.padding) ? this.random.y : (this.height - this.puzzleSize - this.padding)
      } else {
        this.random.y = Math.floor(Math.random() * (this.height - this.puzzleSize - this.padding * 2) + this.padding)
      }
    },
    /* 初始化图片 */
    initImage () {
      this.imageObj = new Image()
      this.imageObj.onload = () => {
        this.isCanvasLoaded = true
        this.initKonva()
      }
      this.imageObj.src = this.images[Math.floor(Math.random() * this.images.length)]
    },
    /* 初始化Konva */
    initKonva () {
      const that = this
      /* 创建两个image, 层级在上的用group裁剪成拼图, 层级在下的用line遮住 */
      const stage = new Konva.Stage({
        container: 'image-verify-canvas',
        width: this.width,
        height: this.height
      })
      this.layer = new Konva.Layer()

      const lineImgae = new Konva.Image({
        image: this.imageObj,
        x: 0,
        y: 0,
        width: that.width,
        height: that.height
      })
      const line = new Konva.Line({
        shadowBlur: 5,
        shadowOffsetX: 0,
        shadowOffsetY: 0,
        x: this.random.x,
        y: this.random.y,
        points: this.setPuzzleType(this.puzzleType),
        bezier: true,
        closed: true,
        stroke: '#aaaaaa',
        strokeWidth: 0.5,
        fill: '#ffffff',
        opacity: 0.9
      })
      const groupBottom = new Konva.Group({
        x: 0,
        y: 0,
        draggable: false
      })
      groupBottom.add(lineImgae)
      groupBottom.add(line)

      this.clipGroup = new Konva.Group({
        x: this.padding,
        y: this.random.y,
        draggable: true,
        clipFunc: function (ctx) {
          ctx.lineWidth = 2
          ctx.strokeStyle = 'white'
          ctx.moveTo(0, 0)
          that.setPuzzleType(that.puzzleType, ctx)
          ctx.stroke()
        },
        dragBoundFunc: function (pos) {
          return {
            x: that.isCanDrag ? pos.x : this.absolutePosition().x,
            y: this.absolutePosition().y
          }
        }
      })

      const image = new Konva.Image({
        image: this.imageObj,
        x: -this.random.x,
        y: -this.random.y,
        width: that.width,
        height: that.height
      })
      this.clipGroup.add(image)
      this.layer.add(this.clipGroup)
      this.layer.draw()
      /* 鼠标hover拼图 */
      this.clipGroup.on('mouseenter', function () {
        that.$refs.img_verify.style.cursor = 'pointer'
      })
      /* 鼠标离开拼图 */
      this.clipGroup.on('mouseout', function () {
        that.$refs.img_verify.style.cursor = 'default'
      })
      /* 开始拖动拼图 */
      this.clipGroup.on('dragstart', function () {
        that.sliderDragAble = that.isCanDrag
      })
      /* 正在拖动拼图 */
      const max = that.width - that.puzzleSize - that.padding
      this.clipGroup.on('dragmove', function () {
        if (that.clipGroup.getAttr('x') < that.padding) {
          that.clipGroup.setAttr('x', that.padding)
        } else if (this.getAttr('x') > max) {
          that.clipGroup.setAttr('x', max)
        }
        that.layer.draw()
        that.$refs.slider_button.style.left = that.clipGroup.getAttr('x') - that.leftGap + 'px'
      })
      /* 结束拖动拼图 */
      this.clipGroup.on('dragend', function () {
        if (!that.isCanDrag) return
        that.sliderDragAble = false
        that.checkIsPass()
      })
      this.layer.add(groupBottom)
      stage.add(this.layer)

      groupBottom.moveToBottom()
      this.layer.draw()
    },
    /* 开始拖动滑块 */
    sliderStart (e) {
      if (!this.isCanDrag) return
      this.sliderDragAble = true
      this.sliderClientX = e.clientX
      this.$refs.img_verify.addEventListener('mousemove', this.sliderDrag)
      this.$refs.img_verify.addEventListener('mouseup', this.sliderEnd)
    },
    /* 正在拖动滑块 */
    sliderDrag (e) {
      // 触摸时clientX为第一个触摸点的clientX
      if (e.type === 'touchmove') e = e.touches[0]
      if (this.sliderDragAble) {
        if (e.clientX - this.sliderClientX < 0) {
          this.$refs.slider_button.style.left = this.sliderBtnStartX + 'px'
          this.clipGroup.setAttr('x', this.padding)
        } else if (e.clientX - this.sliderClientX > (this.width - (this.puzzleSize + this.padding * 2))) {
          this.$refs.slider_button.style.left = this.sliderBtnStartX + this.width - (this.puzzleSize + this.padding * 2) + 'px'
          this.clipGroup.setAttr('x', this.width - this.puzzleSize - this.padding)
        } else {
          const x = e.clientX - this.sliderClientX
          this.$refs.slider_button.style.left = this.sliderBtnStartX + x + 'px'
          this.clipGroup.setAttr('x', x + this.padding)
        }
        this.layer.draw()
      }
    },
    /* 结束拖动滑块 */
    sliderEnd (e) {
      if (this.sliderDragAble) {
        this.sliderDragAble = false
        this.sliderClientX = 0
        if (e.type === 'touchend') {
          this.$refs.img_verify.removeEventListener('touchmove', this.sliderDrag)
          this.$refs.img_verify.removeEventListener('touchend', this.sliderEnd)
        } else {
          this.$refs.img_verify.removeEventListener('mousemove', this.sliderDrag)
          this.$refs.img_verify.removeEventListener('mouseup', this.sliderEnd)
        }
        this.checkIsPass()
      }
    },
    /* 开始触摸 */
    touchStart (e) {
      if (!this.isCanDrag) return
      this.sliderDragAble = true
      this.sliderClientX = e.touches[0].clientX
      this.$refs.img_verify.addEventListener('touchmove', this.sliderDrag)
      this.$refs.img_verify.addEventListener('touchend', this.sliderEnd)
    },
    /* 验证是否通过 */
    checkIsPass () {
      const x = this.clipGroup.getAttr('x')
      if (Math.abs(x - this.random.x) < this.spacing) {
        this.isPassed = true
        this.isCanDrag = false
      } else {
        this.moveToStart()
      }
      this.$emit('validate', this.isPassed, {
        x,
        y: this.random.y
      })
    },
    /* 滑块和拼图回退到初始位置 */
    moveToStart () {
      this.isCanDrag = false
      this.timeoutId = setTimeout(() => {
        this.timeoutId = null
        this.intervalId = setInterval(() => {
          if (this.clipGroup.getAttr('x') < this.padding) {
            clearInterval(this.intervalId)
            this.intervalId = null
            this.clipGroup.setAttr('x', this.padding)
            this.$refs.slider_button.style.left = this.sliderBtnStartX + 'px'
            this.isCanDrag = true
            this.$emit('validated')
          } else {
            this.$refs.slider_button.style.left = this.clipGroup.getAttr('x') - this.leftGap - this.resumeSpeed + 'px'
            this.clipGroup.setAttr('x', this.clipGroup.getAttr('x') - this.resumeSpeed)
          }
          this.layer.draw()
        }, 5)
      }, this.failDelay)
    },
    /* 设置拼图类型及朝向 */
    setPuzzleType (type, c) {
      const p = this.puzzleSize
      const sp = Math.floor((this.puzzleSize - this.outWidth) / 2)
      const ep = p - sp
      const op = this.puzzleSize + this.outHeight
      const ip = this.puzzleSize - this.outHeight
      const oh = this.outHeight
      if (c) {
        switch (type) {
          case 1:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, -oh, ep, -oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(ip, sp, ip, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 2:
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(op, sp, op, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, ip, sp, ip, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 3:
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, op, sp, op, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(oh, ep, oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 4:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, oh, ep, oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(-oh, ep, -oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 5:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, -oh, ep, -oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(oh, ep, oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 6:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, oh, ep, oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(op, sp, op, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 7:
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(ip, sp, ip, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, op, sp, op, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 8:
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, ip, sp, ip, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(-oh, ep, -oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 9:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, -oh, ep, -oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(op, sp, op, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 10:
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(op, sp, op, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, op, sp, op, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 11:
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, op, sp, op, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(-oh, ep, -oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 12:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, -oh, ep, -oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(-oh, ep, -oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 13:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, oh, ep, oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(ip, sp, ip, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 14:
            c.lineTo(p, 0)
            c.lineTo(p, sp)
            c.bezierCurveTo(ip, sp, ip, ep, p, ep)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, ip, sp, ip, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, 0)
            break
          case 15:
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(ep, p)
            c.bezierCurveTo(ep, ip, sp, ip, sp, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(oh, ep, oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
          case 16:
            c.lineTo(sp, 0)
            c.bezierCurveTo(sp, oh, ep, oh, ep, 0)
            c.lineTo(p, 0)
            c.lineTo(p, p)
            c.lineTo(0, p)
            c.lineTo(0, ep)
            c.bezierCurveTo(oh, ep, oh, sp, 0, sp)
            c.lineTo(0, 0)
            break
        }
      } else {
        switch (type) {
          case 1:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, -oh, ep, -oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, sp, p, sp, ip, sp, ip, ep, p, ep, p, ep, p, p, p, p, p, p, 0, p, 0, p]
          case 2:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, sp, p, sp, op, sp, op, ep, p, ep, p, ep, p, p, p, p, p, p, ep, p, ep, p, ep, ip, sp, ip, sp, p, sp, p, 0, p, 0, p]
          case 3:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, ep, p, ep, p, ep, op, sp, op, sp, p, sp, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, oh, ep, oh, sp, 0, sp]
          case 4:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, oh, ep, oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, -oh, ep, -oh, sp, 0, sp]
          case 5:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, -oh, ep, -oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, oh, ep, oh, sp, 0, sp]
          case 6:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, oh, ep, oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, sp, p, sp, op, sp, op, ep, p, ep, p, ep, p, p, p, p, p, p, 0, p, 0, p]
          case 7:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, sp, p, sp, ip, sp, ip, ep, p, ep, p, ep, p, p, p, p, p, p, ep, p, ep, p, ep, op, sp, op, sp, p, sp, p, 0, p, 0, p]
          case 8:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, ep, p, ep, p, ep, ip, sp, ip, sp, p, sp, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, -oh, ep, -oh, sp, 0, sp]
          case 9:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, -oh, ep, -oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, sp, p, sp, op, sp, op, ep, p, ep, p, ep, p, p, p, p, p, p, 0, p, 0, p]
          case 10:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, sp, p, sp, op, sp, op, ep, p, ep, p, ep, p, p, p, p, p, p, ep, p, ep, p, ep, op, sp, op, sp, p, sp, p, 0, p, 0, p]
          case 11:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, ep, p, ep, p, ep, op, sp, op, sp, p, sp, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, -oh, ep, -oh, sp, 0, sp]
          case 12:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, -oh, ep, -oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, -oh, ep, -oh, sp, 0, sp]
          case 13:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, oh, ep, oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, sp, p, sp, ip, sp, ip, ep, p, ep, p, ep, p, p, p, p, p, p, 0, p, 0, p]
          case 14:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, sp, p, sp, ip, sp, ip, ep, p, ep, p, ep, p, p, p, p, p, p, ep, p, ep, p, ep, ip, sp, ip, sp, p, sp, p, 0, p, 0, p]
          case 15:
            return [0, 0, 0, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, ep, p, ep, p, ep, ip, sp, ip, sp, p, sp, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, oh, ep, oh, sp, 0, sp]
          case 16:
            return [0, 0, 0, 0, sp, 0, sp, 0, sp, oh, ep, oh, ep, 0, ep, 0, p, 0, p, 0, p, 0, p, p, p, p, p, p, 0, p, 0, p, 0, p, 0, ep, 0, ep, oh, ep, oh, sp, 0, sp]
        }
      }
    }
  }
});

// CONCATENATED MODULE: ./packages/imageVerify/src/image-verify.vue?vue&type=script&lang=js&
 /* harmony default export */ var src_image_verifyvue_type_script_lang_js_ = (image_verifyvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./packages/imageVerify/src/image-verify.vue?vue&type=style&index=0&id=d49aee4a&lang=scss&scoped=true&
var image_verifyvue_type_style_index_0_id_d49aee4a_lang_scss_scoped_true_ = __webpack_require__("3825");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./packages/imageVerify/src/image-verify.vue






/* normalize component */

var component = normalizeComponent(
  src_image_verifyvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  "d49aee4a",
  null
  
)

/* harmony default export */ var image_verify = (component.exports);
// CONCATENATED MODULE: ./packages/imageVerify/index.js
// 导入组件，组件必须声明 name


// 为组件提供 install 安装方法，供按需引入
image_verify.install = function (Vue) {
  Vue.component(image_verify.name, image_verify)
}

// 默认导出组件
/* harmony default export */ var imageVerify = (image_verify);

// CONCATENATED MODULE: ./packages/index.js
// 导入日期选择器组件


// 存储组件列表
const components = [
  imageVerify
]

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = function (Vue) {
  // 判断是否安装
  if (install.installed) return
  // 遍历注册全局组件
  components.map(component => Vue.component(component.name, component))
}

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

/* harmony default export */ var packages_0 = ({
  // 导出的对象必须具有 install，才能被 Vue.use() 方法安装
  install,
  // 以下是具体的组件列表
  ...components
});

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (packages_0);



/***/ })

/******/ });
});
//# sourceMappingURL=image-verify.umd.js.map